var subscriberData =
{
    labels: ["Subscribers Opted", "Subscribers Viewed"],
    datasets: [

        {
            label: "Female",
            fillColor: "#f26a62",
            strokeColor: "#f26a62",
            highlightFill: "#d85d56",
            highlightStroke: "#d85d56",
            data: [10, 0]
        },
        {
            label: "Male",
            fillColor: "#90d8f4",
            strokeColor: "#90d8f4",
            highlightFill: "#86cae4",
            highlightStroke: "#86cae4",
            data: [0, 90]
        }
    ]
};

var impressionData =
{
   labels: ["25 Jan", "26 Jan", "27 Jan", "28 Jan",
            "29 Jan", "30 Jan", "1 Feb", "2 Feb", "3 Feb",
            "4 Feb", "5 Feb"],
   datasets: [
       {
           label: "Total impressions",
           fillColor: "#f26a62",
           strokeColor: "#f26a62",
           pointColor: "#f26a62",
           pointStrokeColor: "f26a62",
           pointHighlightFill: "f26a62",
           pointHighlightStroke: "#f26a62",
           data: [15, 39, 60, 81, 56, 55, 60, 55, 65, 95,120, 140, 150]
       }
   ]
};
