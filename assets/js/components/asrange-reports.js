
$("#ageGroup").asRange({
        skin: null,
        max: 100,
        min: 0,
        value: [18, 35],
        step: 1,
        limit: true,
        orientation: 'h',
        tip: false,
        scale:false,
        format: function(value) {
            return value;
        },
        onChange: function(instance) {
          $("#ageLimitMin").val(instance[0]);
          $("#ageLimitMax").val(instance[1]);
          $("#ageGroup .rangeUi-pointer-1").html(instance[0]);
          $("#ageGroup .rangeUi-pointer-2").html(instance[1]);
        },
        callback: function() {}
});
$("#ageGroup .rangeUi-pointer-1").html("18");
$("#ageGroup .rangeUi-pointer-2").html("35");
$("#ageLimitMin").val(18);
$("#ageLimitMax").val(35);
