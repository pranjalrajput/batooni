
/*$("#timeSlot").asRange({
        skin: null,
        max: 1440,
        min: 0,
        value: [660, 840],
        step: 5,
        limit: true,
        orientation: 'h',
        tip: false,
        scale:false,
        format: function(value) {
            return value;
        },
        onChange: function(instance) {
          var start = Math.floor(instance[0]/60) + ":"+instance[0]%60;
          var end = Math.floor(instance[1]/60) + ":"+instance[1]%60;
          $("#timeSlotStartVal").val(instance[0]);
          $("#timeSlotStart").val(
            moment("12-01-2016 "+start, "MM-DD-YYYY H:m")
            .format("hh:mm a")
          );
          $("#timeSlot .rangeUi-pointer-1").html(start);

          $("#timeSlotEndVal").val(instance[1]);
          $("#timeSlotEnd").val(
            moment("12-01-2016 "+end, "MM-DD-YYYY H:m")
            .format("hh:mm a")
          );
          $("#timeSlot .rangeUi-pointer-2").html(end);

          //--move time slot with slider
          var left1 = $("#timeSlot .rangeUi-pointer-1").attr("style").replace("left: ","").replace("%","").replace(";","");
          var left2 = $("#timeSlot .rangeUi-pointer-2").attr("style").replace("left: ","").replace("%","").replace(";","");
          var leftMiddle = (parseFloat(left1) + parseFloat(left2))/2;
          $("#timeSlotDisplay").css("left",leftMiddle+"%");
        },
        callback: function() {}
});
//set initial values for time slot
$("#timeSlot .rangeUi-pointer-1").html("11:00");
$("#timeSlot .rangeUi-pointer-2").html("14:00");
$("#timeSlotStart").val("11:00 am");
$("#timeSlotEnd").val("02:00 pm");
$("#timeSlotStartVal").val(660);
$("#timeSlotEndVal").val(840);
*/
$("#ageGroup").asRange({
        skin: null,
        max: 100,
        min: 0,
        value: [18, 35],
        step: 1,
        limit: true,
        orientation: 'h',
        tip: false,
        scale:false,
        format: function(value) {
            return value;
        },
        onChange: function(instance) {
          $("#ageLimitMin").val(instance[0]);
          $("#ageLimitMax").val(instance[1]);

          //--move time slot with slider
          var left1 = $("#ageGroup .rangeUi-pointer-1").attr("style").replace("left: ","").replace("%","").replace(";","");
          var left2 = $("#ageGroup .rangeUi-pointer-2").attr("style").replace("left: ","").replace("%","").replace(";","");
          var leftMiddle = (parseFloat(left1) + parseFloat(left2))/2;
          $("#ageSlotDisplay").css("left",leftMiddle+"%");
        },
        callback: function() {}
});
$("#ageGroup .rangeUi-pointer-1").html("18");
$("#ageGroup .rangeUi-pointer-2").html("35");
$("#ageLimitMin").val(18);
$("#ageLimitMax").val(35);

/*$("#totalTarget").asRange({
        skin: null,
        max: 10000,
        min: 0,
        value: [5000],
        step: 100,
        limit: true,
        orientation: 'h',
        tip: false,
        scale:false,
        format: function(value) {
            return value;
        },
        onChange: function(instance) {
          return false;
        },
        callback: function() {}
});*/

$("#totalTargetMale").asRange({
        skin: null,
        max: 5000,
        min: 0,
        value: [3000],
        step: 100,
        limit: true,
        orientation: 'h',
        tip: false,
        scale:false,
        format: function(value) {
            return value;
        },
        onChange: function(instance) {
          var localVal = parseInt(instance).toLocaleString();
          $("#totalTargetMale .rangeUi-pointer-1").html(localVal);
          $("#totalTargetMaleVal").val(instance);
          $("#maleStat").html(localVal);
          donutChart.segments[0].value = instance;
          donutChart.update();
          var females = $("#totalTargetFemaleVal").val();
          var total = parseInt(females) + parseInt(instance);
          var percent = total/100.00;
          $("#totalTarget .rangeUi-pointer-1")
            .html(total.toLocaleString())
            .css("left",percent+"%");
          $("#totalTarget .rangeUi-selected").css("width",percent+"%");
        },
        callback: function() {}
});

var males = 3000;
$("#totalTargetMale .rangeUi-pointer-1").html(males.toLocaleString());
$("#totalTargetMaleVal").val(males);
$("#maleStat").html(males.toLocaleString());

$("#totalTargetFemale").asRange({
        skin: null,
        max: 5000,
        min: 0,
        value: [2000],
        step: 100,
        limit: true,
        orientation: 'h',
        tip: false,
        scale:false,
        format: function(value) {
            return value;
        },
        onChange: function(instance) {
          var localVal = parseInt(instance).toLocaleString();
          $("#totalTargetFemale .rangeUi-pointer-1").html(localVal);
          $("#totalTargetFemaleVal").val(instance);
          $("#femaleStat").html(localVal);
          donutChart.segments[1].value = instance;
          donutChart.update();
          var males = $("#totalTargetMaleVal").val();
          var total = parseInt(males) + parseInt(instance);
          var percent = total/100.00;
          $("#totalTarget .rangeUi-pointer-1")
            .html(total.toLocaleString())
            .css("left",percent+"%");
          $("#totalTarget .rangeUi-selected").css("width",percent+"%");
        },
        callback: function() {}
});
var females = 2000;
$("#totalTargetFemale .rangeUi-pointer-1").html(females.toLocaleString());
$("#totalTargetFemaleVal").val(females);
$("#femaleStat").html(females.toLocaleString());
