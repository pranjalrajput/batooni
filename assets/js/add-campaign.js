var CITY_LIST =
[
  {
    value: 'Bhopal, Madhya Pradesh'
  }, {
    value: 'Indore, Madhya Pradesh'
  }, {
    value: 'Dewas, Madhya Pradesh'
  }, {
    value: 'Delhi, Delhi'
  }, {
    value: 'Lucknow, UP'
  }, {
    value: 'Jaipur, Rajshtan'
  }
];

(function(document, window, $) {
  'use strict';

  var Site = window.Site;

  $(document).ready(function($) {
    Site.run();
    initScrollTo();
    initArrowBlock();
  });

  initFormValidation();

  initFlicker();

  //-----location token field
  var engine = new Bloodhound({
      local: CITY_LIST,
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
      queryTokenizer: Bloodhound.tokenizers.whitespace
    });

    // engine.initialize();

    $('#inputTokenfieldTypeahead').tokenfield({
      typeahead: [null, {
        name: 'engine',
        displayKey: 'value',
        source: engine.ttAdapter()
      }]
    });


    //----------set date vs set days
    $("#setDates").on("change",function(){
      if( $(this).prop("checked") )
      {
        $(".days-filter-buttons").fadeOut();
        $("#setDaysTab").slideUp();
        $("#setDatesTab").slideDown();
      }
    });
    $("#setDays").on("change",function(){
      if( $(this).prop("checked") )
      {
        $("#setDatesTab").slideUp();
        $(".days-filter-buttons").fadeIn();
        $("#setDaysTab").slideDown();
      }

    });

    //----- datepicker initialize
    $('#campaignDatePicker').datepicker(
      {
        "multidate":true
      }
    );
    $("#campaignDatePicker").on("changeDate", function(event) {
      $("#campaignDateValues").val(
        $("#campaignDatePicker").datepicker('getFormattedDate')
      );
    });

    //--------day filters------------
    $(".days .day").on("click", function(){
        var item = $(this);
        if(item.hasClass("selected"))
        {
          item.removeClass("selected");
        }
        else
        {
          item.addClass("selected");
        }
    });

    $("#allDays").on("click", function(){
      $("#weekends").removeClass("shown");
      if($("#allDays").hasClass("shown"))
      {
        $(".days .day").removeClass("selected");
        $("#allDays").removeClass("shown");
      }
      else {
        $(".days .day").addClass("selected");
        $("#allDays").addClass("shown");
      }
      console.log("call js-grid re-filter here");
    });

    $("#weekends").on("click", function(){
      $(".days .day").removeClass("selected");
      $("#allDays").removeClass("shown");
      if($("#weekends").hasClass("shown"))
      {
        $(".days .day.weekend").removeClass("selected");
        $("#weekends").removeClass("shown");
      }
      else {
        $(".days .day.weekend").addClass("selected");
        $("#weekends").addClass("shown");
      }
      console.log("call js-grid re-filter here");
    });


  $("#newCampaign").on("click", function(){
    window.location = "newCampaign.html";
  });

  //---add slot
  $("#addSlotBtn").on("click", function(){
    if(!checkOverlap())
    {
      $(".time-slot-container").append(
        '<div class="added-time-slot">'+
          '<div class="slot-time">'+
            '<div class="start" data-val="'+$("#timeSlotStartVal").val()+'">'+$("#timeSlotStart").val()+'</div>'+
            '<div class="to">to</div>'+
            '<div class="end" data-val="'+$("#timeSlotEndVal").val()+'">'+$("#timeSlotEnd").val()+'</div>'+
            '<div class="delete-slot"></div>'+
          '</div>'+
        '</div>'
      );
    }
  });

  //---delete slot
  $(".time-slot-container").on("click",".slot-time .delete-slot", function(){
    $(this).parent().parent().remove();
  });

  //--------top buttons ----
  $("#preview").on("click",function(){
    previewVisible = true;
    window.scrollTo(0,0);
    $('body').scrollTo(0);
    $("body").addClass("modal-visible");
    $(".preview-page").addClass("visible");
    if( !$(".preview-summary-container").hasClass("scrollable") )
    {
      $(".preview-summary-container").height($(window).height()-80);
      $(".preview-summary-container")
        .asScrollable({
          namespace: "scrollable",
          contentSelector: "> [data-role='content']",
          containerSelector: "> [data-role='container']"
        });
    }
  });

  $(".button-back").on("click",function(){
    previewVisible = false;
    $(".preview-page").removeClass("visible");
    $("body").removeClass("modal-visible");
  });

  $("#save").on("click",function(){
    console.log("implement save ...");
    var formObject = formInstance.data('formValidation');
    /*--- validate campaign name-----*/
    formObject.validateField('campName');
    if(formObject.isValidField('campName')!=true)
    {
       $("body,html").scrollTo("#campName");
    }
    /*---end validate campaign name-----*/


    /*
    * add fields in the same manner as campName
    */

    if(formObject.isValid()){
      $(".open-modal-dlg").trigger("click");
    }

  });

  $("#launch").on("click",function(){
    console.log("implement launch ...");
    $(".open-confirm-modal-dlg").trigger("click");
  });

  //-------video play
  $(".answer.video").on("click",function(){
    $(".video-overlay").fadeOut(300,"linear",function(){
      $(".video-wrap").show();
    });
  });

  //setup file upload
  initFileUpload();

  initChart();

  initDateRange();

  initSlider();

  initSmoothScroll();

  setTimeout( function(){
    $(".summary-container").height($(window).height()-195);
  },100);

})(document, window, jQuery);

//-----------timelsot overlap
function checkOverlap(){
  var startVal = parseInt( $("#timeSlotStartVal").val() );
  var endVal = parseInt( $("#timeSlotEndVal").val() );
  var isOverlapping = false;

  $(".added-time-slot .slot-time").each(function(i ,ele){

    var thisStart = parseInt( $(ele).find(".start").attr("data-val") );
    var thisEnd = parseInt( $(ele).find(".end").attr("data-val") );

    if(
        (startVal < thisStart && endVal > thisStart)
        ||
        (startVal > thisStart && startVal < thisEnd)
        ||
        (startVal == thisStart && endVal == thisEnd)
      )
    {
      isOverlapping = true;
    }
  });

  return isOverlapping;
}

//------------server error-----------
var hidingInProgress = 0 ;
var isHidden = 1 ;
function serverErrorShow(message)
{
  var icon = "<span class='warn-icon'></span>";
  if(message == undefined || message == "")
    message = "Sorry, something went wrong. Please try later."
  $("#serverError").html(icon+message);
  $(".server-error").animate(
                        {"top":"0px"},
                        500,
                        "swing",
                        function(){
                            isHidden = 0;

                        });
}
function serverErrorHide()
{
  hidingInProgress = 1;
  $(".server-error").animate(
                      {"top":"-50px"},
                      500,
                      "swing",
                      function(){
                        isHidden = 1;
                        hidingInProgress = 0;
                      });
}

//--------progress bar--------------
/*
  for help, please visit - http://ricostacruz.com/nprogress/
  example funcation calls
  NProgress.start();
  NProgress.set(0.4);
  NProgress.inc();
  NProgress.done();
*/

  // date range picker
function initDateRange()
{
    $('input[name="daterange"]').daterangepicker(
    {
      autoUpdateInput: false,
      locale: {
        cancelLabel: 'Cancel',
        applyLabel : 'Apply'
      }
    },
    function(start, end, label) {
        console.log("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
    });

    $('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('MMMM DD, YYYY')
        + ' - '
        + picker.endDate.format('MMMM DD, YYYY'));
    });

    $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    $(window).on("scroll", function(){
      $("input[name='daterange']").blur().trigger("blur");
    });
}

//  File Upload
// -------------------
var dropZone;
var currentFile;
var videoUrl;
function initFileUpload()
{
    /*$('#inputUpload').fileupload({
        dataType: 'json',
        done: function (e, data)
        {
            $.each(data.result.files, function (index, file)
            {
                  $(".thumb-wrap img").attr("src", file.thumbnailUrl);
                  $(".hide-after").fadeOut(500, function(){$(".thumb-wrap").fadeIn(500)});
            });
        },
        progressall: function (e, data)
        {

          var progress = parseInt(data.loaded / data.total * 100, 10);
          console.log('progress-'+progress);
          $('.upload-progress .bar').css(
              'width',
              progress + '%'
          );
        }
  });

  $(".uploader-inline").on("click", function(){
    $("#inputUpload").trigger("click");
  });*/

  var dropZone  = $("#uploadForm").dropzone(
    {
      url: "php/",
      maxFileSize:1024*1024,
      previewTemplate:"<div></div>",
      clickable:['.file-upload-section'],
      utoProcessQueue:true,
      paramName:'files',
      init: function()
      {
        this.on("addedfile",
                  function(file)
                  {
                    console.log(file);
                    currentFile = file;
                    $(".uploading-file-name").html(file.name);
                    $("#totalBytes").html(filesize(file.size));
                    $(".hide-during").hide();
                    $(".show-during").show();
                  }
                );
        this.on("uploadprogress",
                function(file,percent,bytesSent)
                {
                  console.log(percent+"%, "+bytesSent);
                  $('.upload-progress .bar').css(
                      'width',
                      percent + '%'
                  );
                  $("#doneBytes").html(filesize(bytesSent));
                }
              );
        this.on("success",
                function(file,response)
                {
                  console.log(response.files[0].thumb);
                  console.log(response.files[0].url);

                  $(".hide-after").hide();
                  //$(".show-after .thumb-wrap").attr("src",response.files[0].thumb);
                  $(".show-after .thumb-wrap img").attr("src","assets/img/video-thumb.jpg");
                  $(".show-after").show();
                  videoUrl = response.files[0].url;
                }
              );
      }
    }
  );

  //---cancel upload--
  $(".cancel-upload").on("click", function(){
    console.log("call remove file here");
  });
  $(".thumb-wrap").on("click", function(){
    $(".show-after").hide();
    $(".video-player").show();
    var video = document.createElement('video');
    $(video).attr("width","495").attr("height","220");
    $(".video-wrap-live").append(video);
    var source = document.createElement('source');
    source.src = videoUrl;
    video.appendChild(source);
    video.play();
  });

}
var donutChart;
function initChart()
{
    var doughnutData = [
    {
      value: 0,
      color: "#90d8f4",
      fillColor: "#90d8f4",
      strokeColor: "#90d8f4",
      label: "Male"
    },
    {
      value: 1000,
      color: "#f26a62",
      fillColor: "#f26a62",
      strokeColor: "#f26a62",
      label: "Female"
    },
    {
      value: 1000,
      color: "#7e94a9",
      fillColor: "#7e94a9",
      strokeColor: "#7e94a9",
      label: ""
    }];

    donutChart = new Chart(
          document
          .getElementById("donutChart")
          .getContext("2d")
        )
        .Doughnut(
          doughnutData,
          {
            segmentShowStroke : false,
            showTooltips: true,
            customTooltips: function(tooltip) {
                if (!tooltip)
                {
                    return;
                }
                else
                {
                  if(doughnutData[0].value ==0 && doughnutData[1].value ==0)
                  {
                    return;
                  }
                }
            }
          }
        ) ;


}

function initScrollTo(){

  $.fn.scrollTo = function( target, options, callback ){
    if(typeof options == 'function' && arguments.length == 2){ callback = options; options = target; }
    var settings = $.extend({
      scrollTarget  : target,
      offsetTop     : 160,
      duration      : 500,
      easing        : 'swing'
    }, options);
    return this.each(function(){
      var scrollPane = $(this);
      var scrollTarget = (typeof settings.scrollTarget == "number") ? settings.scrollTarget : $(settings.scrollTarget);
      var scrollY = (typeof scrollTarget == "number") ? scrollTarget : scrollTarget.offset().top + scrollPane.scrollTop() - parseInt(settings.offsetTop);
      scrollPane.animate({scrollTop : scrollY }, parseInt(settings.duration), settings.easing, function(){
        if (typeof callback == 'function') { callback.call(this); }
      });
    });
  }
}

function initSlider(){
  $("#timeSlot").slider(
    {
      range: true,
      min: 0,
      max: 1440,
      values: [660, 840],
      step: 5,
      slide: function( event, ui ) {
        var instance  = ui.values;
        var start = Math.floor(instance[0]/60) + ":"+instance[0]%60;
        var end = Math.floor(instance[1]/60) + ":"+instance[1]%60;
        $("#timeSlotStartVal").val(instance[0]);
        $("#timeSlotStart").val(
          moment("12-01-2016 "+start, "MM-DD-YYYY H:m")
          .format("hh:mm a")
        );
        $("#timeSlot .ui-slider-handle:nth-child(2)").html(moment("12-01-2016 "+start, "MM-DD-YYYY H:m")
        .format("hh:mm"));

        $("#timeSlotEndVal").val(instance[1]);
        $("#timeSlotEnd").val(
          moment("12-01-2016 "+end, "MM-DD-YYYY H:m")
          .format("hh:mm a")
        );
        $("#timeSlot .ui-slider-handle:nth-child(3)").html(  moment("12-01-2016 "+end, "MM-DD-YYYY H:m")
          .format("hh:mm"));

        //--move time slot with slider
        var left1 = $("#timeSlot .ui-slider-handle:nth-child(2)").attr("style").replace("left: ","").replace("%","").replace(";","");
        var left2 = $("#timeSlot .ui-slider-handle:nth-child(3)").attr("style").replace("left: ","").replace("%","").replace(";","");
        var leftMiddle = (parseFloat(left1) + parseFloat(left2))/2;
        $("#timeSlotDisplay").css("left",leftMiddle+"%");

        $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
      }
    }
  );
  //set initial values for time slot
  $("#timeSlot .ui-slider-handle:nth-child(2)").html("11:00");
  $("#timeSlot .ui-slider-handle:nth-child(3)").html("14:00");
  $("#timeSlotStart").val("11:00 am");
  $("#timeSlotEnd").val("02:00 pm");
  $("#timeSlotStartVal").val(660);
  $("#timeSlotEndVal").val(840);

}

var lastScrollTop = 0;
var isShownAudience = false;
var isShownLimits = false;
var isShownTop = false;
function initSmoothScroll(){
  $(window).scroll(function(event){
    //--------conditional on up or down scroll
    var st = $(this).scrollTop();
    if (st > lastScrollTop)
    {
      // downscroll code
      if(isScrolledIntoView("#targetAudienceMarker") && isShownAudience == false)
      {
        $(".summary-container").asScrollable('scrollBy', 'vertical', 174);
        isShownAudience = true;
        isShownLimits = false;
        isShownTop = false;
      }
      if(isScrolledIntoView("#limitsSectionMarker") && isShownLimits == false)
      {
        $(".summary-container").asScrollable('scrollBy', 'vertical', 143);
        isShownAudience = false;
        isShownLimits = true;
        isShownTop = false;
      }
    }
    else {
      // upscroll code
      console.log("down");
    }
    lastScrollTop = st;

    //-------all scroll events

  });
}

function isScrolledIntoView(elem)
{
    var $elem = $(elem);
    var $window = $(window);

    var docViewTop = $window.scrollTop();
    var docViewBottom = docViewTop + $window.height();

    var elemTop = $elem.offset().top;
    var elemBottom = elemTop + $elem.height();

    if ((elemBottom <= docViewBottom) && (elemTop >= docViewTop))
    {
      if(elemTop - docViewTop < $window.height()/2)
      {
        return true;
      }
    }
    return false;
}

var formInstance;
function initFormValidation(){
  formInstance = $('#addCampaignForm').formValidation(
    {
      framework: "bootstrap",
      button:
      {
        selector: '#loginSubmit',
        disabled: 'disabled'
      },
      icon: null,
      live:'disabled',
      fields:
      {
        'campName':
        {
          validators:
          {
            notEmpty:
            {
              message: 'The name is required'
            },
            regexp:
            {
              regexp: /^[a-zA-Z0-9]+$/
            }
          }
        }
      }
  });
  formInstance.on('success.form.fv', function(e) {
      var fv  = $(e.target).data('formValidation');
      fv.defaultSubmit();
  });

}
function initFlicker(){
  $("#timeSlotLeft").on("click", function(){
    var timeSlot1 = $("#timeSlot").slider('values')[0];
    var timeSlot2 = $("#timeSlot").slider('values')[1];
    if(timeSlot1>100)
    {
      $("#timeSlot").slider('values',0,timeSlot1-100);
      $("#timeSlot").slider('values',1,timeSlot2-100);
    }
  });
  $("#timeSlotRight").on("click", function(){
    var timeSlot1 = $("#timeSlot").slider('values')[0];
    var timeSlot2 = $("#timeSlot").slider('values')[1];
    if(timeSlot2<1340)
    {
      $("#timeSlot").slider('values',0,timeSlot1+100);
      $("#timeSlot").slider('values',1,timeSlot2+100);
    }
  });
  $("#ageLeft").on("click", function(){});
  $("#ageLeft").on("click", function(){});
}
//block the arrow navigation on firefox
var previewVisible = false;
function initArrowBlock()
{
  window.addEventListener(
    "keydown",
    function(e)
    {
      if(previewVisible==true)
      {
        // space and arrow keys
        if([32, 37, 38, 39, 40].indexOf(e.keyCode) > -1)
        {
              e.preventDefault();
        }
      }
    },
    false);


}
