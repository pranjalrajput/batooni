
(function(document, window, $) {
  'use strict';

  var Site = window.Site;

  $(document).ready(function($) {
    Site.run();
  });


  //--------------------
  // JS grid
  (function() {
    //var availableHeight = $(window).height() - 160 -152 - 50;
    //var rowCount = Math.floor(availableHeight / 41);

    var jsGrid =
    $('#dataTable').jsGrid({
      width: "100%",

      filtering: false,
      editing: false,
      sorting: true,
      paging: true,
      autoload: true,

      pageSize: 10,
      pageButtonCount: 5,

      deleteConfirm: "Do you really want to delete the client?",

      controller: db,

      fields:
      [
        {
          name: "Client",
          type: "text",
          width: "17.15%"
        },
        {
          name: "Brand",
          type: "text",
          width: "14.55%"
        },
        {
          name: "Campaign",
          type: "text",
          width: "22.85%"
        },
        {
          title: "Start Date",
          name: "StartDate",
          type: "date",
          width: "16.55%"
        },
        {
          title: "End Date",
          name: "EndDate",
          type: "date",
          width: "16.55%"
        },
        {
          name: "Status",
          type: "text",
          width: "12.25%"
        }
      ]
      ,

      rowRenderer: function(item) {
        var rowString  = "<tr>" ;

        rowString += "<td style='width:17.15%'>"+item.Client +"</td>"+
                     "<td style='width:14.55%'>"+item.Brand +"</td>"+
                     "<td style='width:22.85%'>"+item.Campaign +"</td>"+
                     "<td style='width:16.55%'>"+item.StartDate +"</td>"+
                     "<td style='width:16.55%'>"+item.EndDate +"</td>"+
                     "<td style='width:12.25%' class='"+item.Status+"'>"+item.Status +"</td>";
        return rowString;
      }

    });

    //default sort by
    $('#dataTable').jsGrid("sort", { field: "StartDate", order: "desc" });
  })();


  //show/hide more filters
  $(".toggle-filters").on("click", function(){
    var item = $(this);
    if(item.hasClass("shown")){
      $(".more-filters").slideUp(500, function(){$(".toggle-filters").removeClass("shown");});
    }
    else {
      $(".more-filters").slideDown(500, function(){$(".toggle-filters").addClass("shown");});
    }
  });

  $(".reset-filters").on("click", function(){
    $("#client").val("").trigger("blur");
    $("#brand").val("").trigger("blur");
    $("#campaign").val("").trigger("blur");
    $("#location").val("").trigger("blur");
    $("#startDate").datepicker("clearDates");
    $("#endDate").datepicker("clearDates");
    $(".days-filter .day").removeClass("selected");
    $("#allDays").removeClass("shown");
    $(".days-filter .day.weekend").removeClass("selected");
    $("#weekends").removeClass("shown");
  });
  $("#buttonSearch").on("click", function(){
    $('#dataTable').jsGrid(
        "search",
        {
          "Client"    :  $("#client").val(),
          "Brand"     :  $("#brand").val(),
          "Campaign"  :  $("#campaing").val(),
          "Location"  :  $("#location").val(),
          "StartDate" :  $("#startDate").val(),
          "EndDate"   :  $("#endDate").val()
        });
  });


  $("#startDate").on("changeDate", function(){
    //console.log("changeDate");
    //set min date for toDate
    $("#endDate").datepicker("setStartDate", $("#startDate").datepicker("getDate"));
    $(".clear-start-date").show();
  });
  $("#startDate").on("clearDate", function(){
    //console.log("clearDate");
    //clear min date for toDate
    $("#endDate").datepicker("setStartDate");
    $(".clear-start-date").hide();
  });
  $("#startDate").on("hide", function(){
    //console.log("hide");
  });

  $("#endDate").on("changeDate", function(){
    $(".clear-end-date").show();
  });
  $("#endDate").on("clearDate", function(){
    $(".clear-end-date").hide();
  });
  $("#endDate").on("hide", function(){
    //console.log("hide");
  });

  $(".clear-start-date").on("click", function(){
    $("#startDate").datepicker("clearDates");
    $(".clear-start-date").hide();
  });
  $(".clear-end-date").on("click", function(){
    $("#endDate").datepicker("clearDates");
    $(".clear-end-date").hide();
  });
  //--------day filters------------
  $(".days-filter .day").on("click", function(){
      var item = $(this);
      if(item.hasClass("selected"))
      {
        item.removeClass("selected");
      }
      else
      {
        item.addClass("selected");
      }
  });

  $("#allDays").on("click", function(){
    $("#weekends").removeClass("shown");
    if($("#allDays").hasClass("shown"))
    {
      $(".days-filter .day").removeClass("selected");
      $("#allDays").removeClass("shown");
    }
    else {
      $(".days-filter .day").addClass("selected");
      $("#allDays").addClass("shown");
    }
  });

  $("#weekends").on("click", function(){
    $(".days-filter .day").removeClass("selected");
    $("#allDays").removeClass("shown");
    if($("#weekends").hasClass("shown"))
    {
      $(".days-filter .day.weekend").removeClass("selected");
      $("#weekends").removeClass("shown");
    }
    else {
      $(".days-filter .day.weekend").addClass("selected");
      $("#weekends").addClass("shown");
    }
  });

  $("#newCampaign").on("click", function(){
    window.location = "newCampaign.html";
  });

  $("#location").select2({
    minimumResultsForSearch: Infinity,
    tags: true
  });

})(document, window, jQuery);
var pageCountSelectedValue=10;
function bindPageCountSelector(){
  //------pagination count selector
  $(".pageCountSelect select").on("change",function(){
    var value = $(this).find("option:selected").val();
    pageCountSelectedValue = value;
    $("#dataTable").jsGrid("option", "pageSize", value);
  });
}
//------------server error-----------
var hidingInProgress = 0 ;
var isHidden = 1 ;
function serverErrorShow(message)
{
  if(message == undefined || message == "")
    message = "Sorry, something went wrong. Please try later."
  $("#serverError").html(message);
  $(".server-error").animate(
                        {"top":"0px"},
                        500,
                        "swing",
                        function(){
                            isHidden = 0;

                        });
}
function serverErrorHide()
{
  hidingInProgress = 1;
  $(".server-error").animate(
                      {"top":"-50px"},
                      500,
                      "swing",
                      function(){
                        isHidden = 1;
                        hidingInProgress = 0;
                      });
}

//--------progress bar--------------
/*
  for help, please visit - http://ricostacruz.com/nprogress/
  example funcation calls
  NProgress.start();
  NProgress.set(0.4);
  NProgress.inc();
  NProgress.done();
*/
