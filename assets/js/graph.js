
(function(document, window, $) {
  'use strict';

  var Site = window.Site;

  $(document).ready(function($) {
    Site.run();
  });


  //--------------------
  // JS grid
  (function() {

    var jsGrid =
    $('#dataTable').jsGrid({
      width: "100%",

      filtering: false,
      editing: false,
      sorting: true,
      paging: true,
      autoload: true,

      pageSize: 10,
      pageButtonCount: 5,

      controller: db,

      fields:
      [
        {
          name: "Country",
          type: "text",
          width: "25%"
        },
        {
          name: "State",
          type: "text",
          width: "25%"
        },
        {
          name: "City",
          type: "text",
          width: "25%"
        },
        {
          title: "Impressions",
          name: "Impression",
          type: "number",
          width: "25%"
        }
      ]
      ,

      rowRenderer: function(item) {
        var rowString  = "<tr>" ;

        rowString += "<td style='width:25%'>"+item.Country +"</td>"+
                     "<td style='width:25%'>"+item.State +"</td>"+
                     "<td style='width:25%'>"+item.City +"</td>"+
                     "<td style='width:25%'>"+item.Impression +"</td>"+
                     "</tr>";
        return rowString;
      }

    });

    //default sort by
    $('#dataTable').jsGrid("sort", { field: "Impression", order: "desc" });
  })();



  $("#buttonSearch").on("click", function(){
    $('#dataTable').jsGrid(
        "search",
        {
          "Client"    :  $("#client").val(),
          "Brand"     :  $("#brand").val(),
          "Campaign"  :  $("#campaing").val(),
          "Location"  :  $("#location").val(),
          "StartDate" :  $("#startDate").val(),
          "EndDate"   :  $("#endDate").val()
        });
  });

  $(".reset-filters").on("click", function(){
    $(".token-input.tt-hint").val("").trigger("blur");
    $("#location").val("").trigger("blur");
    $("#startDate").datepicker("clearDates");
    $("#endDate").datepicker("clearDates");
    $(".gender-selector .check").removeClass("checked");
    $("input[name='gender']").removeAttr("checked");
  });

  $("#startDate").on("changeDate", function(){
    //console.log("changeDate");
    //set min date for toDate
    $("#endDate").datepicker("setStartDate", $("#startDate").datepicker("getDate"));
  });
  $("#startDate").on("clearDate", function(){
    //console.log("clearDate");
    //clear min date for toDate
    $("#endDate").datepicker("setStartDate");
  });
  $("#startDate").on("hide", function(){
    //console.log("hide");
  });
  $(".clear-start-date").on("click", function(){
    $("#startDate").datepicker("clearDates");
  });
  $(".clear-end-date").on("click", function(){
    $("#endDate").datepicker("clearDates");
  });

  //--------location tag field/type ahead------------
  var engine = new Bloodhound({
      local: CITY_LIST,
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
      queryTokenizer: Bloodhound.tokenizers.whitespace
    });

  // engine.initialize();

  $('#location').tokenfield({
    typeahead: [null, {
      name: 'engine',
      displayKey: 'value',
      source: engine.ttAdapter()
    }]
  });

  //----gender selection
  $(".gender-selector .check").on("click",function(){
    var item = $(this);
    var target = "#"+item.attr("data-target");
    $(".gender-selector .check").removeClass("checked");
    $(".gender-selector .check input").removeAttr("checked");
    $(target).attr("checked","");
    item.addClass("checked");
  });

  $("#newCampaign").on("click", function(){
    window.location = "newCampaign.html";
  });

  initChartNew();

  bindCollapse();

})(document, window, jQuery);

function initChartNew()
{
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);
}

function drawChart(){

  drawColumnChart();
  drawLineChart();

}
function drawColumnChart(){
  var dataArray =
    [
        ['Viewer',             'Male'  ,{ role: 'style' },  'Female', { role: 'style' }],
        ['Subscribers\nOpted',  0,    '#90d8f4',            0,      '#f26a62'        ],
        ['Subscribers\nViewed', 16,    '#90d8f4',            22,      '#f26a62'        ]
    ];
  var data = google.visualization.arrayToDataTable(dataArray);

    var options =
    {
       height: 245,
       legend: { position: "none" },
       backgroundColor: 'transparent',
       tooltip: {isHtml: true},
       bar: { groupWidth: '40' },
       focusTarget: 'category',
       colors: ['#90d8f4','#f26a62'],
       chartArea:
              {
                left:'5%',
                top:10,
                width:'90%',
                height:205
              },
       fontSize :10,
       fontName : 'Roboto',
       hAxis: {
                color: '#555555',
                fontName: 'Roboto',
                fontSize: 10,
                gridlines: {color:'#e7f0f3'}
              },
       isStacked: true,
       vAxis :
              {
                baselineColor:'#e7f0f3',
                gridlines: {color:'transparent'},
                minorGridlines: null
              }
    };

  var chart = new google.visualization.ColumnChart(document.getElementById("subsChartCanvas"));
  chart.draw(data, options);
  google.visualization.events
    .addListener(
      chart,
      'onmouseover',
      function(e)
      {
        $("#subsChartCanvas").css({"cursor":"pointer"});
        if( dataArray[e.row+1][1]==0&&dataArray[e.row+1][3]==0 )
        {
          $("div.google-visualization-tooltip").hide();
        }
        else {
          $("div.google-visualization-tooltip").show();
        }
      }
    );
  google.visualization.events
    .addListener(
      chart,
      'onmouseout',
      function(e)
      {
        $("#subsChartCanvas").css({"cursor":""});
      }
    );
  google.visualization.events
    .addListener(
      chart,
      'select',
      function()
      {
        chart.setSelection(null);
      }
    );
}


function drawLineChart()
{
  var data = new google.visualization.DataTable();
  data.addColumn('date', 'Date');
  data.addColumn('number', 'Impressions');
  data.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true} } );
  data.addRows([
    [new Date(2016,0,24), 400, getTooltip("24 Jan", 400)],
    [new Date(2016,0,25), 200, getTooltip("25 Jan", 200)],
    [new Date(2016,0,26), 600, getTooltip("26 Jan", 600)],
    [new Date(2016,0,27), 700, getTooltip("27 Jan", 700)],
    [new Date(2016,0,28), 1150, getTooltip("28 Jan", 1150)],
    [new Date(2016,0,29), 1100., getTooltip("29 Jan", 1100)]
  ]);

  var date_formatter = new google.visualization.DateFormat({
    pattern: "d MMM"
  });
  date_formatter.format(data, 0);

  var options =
  {
    height: 245,
    legend: { position: "none" },
    backgroundColor: 'transparent',
    tooltip: {isHtml: true},
    chartArea:
          {
            left:'5%',
            top:10,
            width:'90%',
            height:205
          },
    fontSize :10,
    fontName : 'Roboto',
    hAxis:
    {
        baselineColor:'#e7f0f3',
        color: '#555555',
        fontName: 'Roboto',
        fontSize: 10,
        gridlines : { color:'transparent' },
        format : 'd MMM',
        ticks: [
          {v: new Date(2016,0,24), f: ''},
          new Date(2016,0,25),
          new Date(2016,0,26),
          new Date(2016,0,27),
          new Date(2016,0,28),
          new Date(2016,0,29)
        ]
    },
    vAxis:
    {
       baselineColor:'#e7f0f3',
       format : '#,###',
       gridlines: { color:'transparent' },
       ticks: [0,200,400,600,800,1000,1200]
    },
    series:
    {
      0:
      {
        color:'#f26a62',
        lineWidth: 3
      }
    }
  };

  var chart = new google.visualization.LineChart(document.getElementById('impChartCanvas'));

  chart.draw(data, options);

  google.visualization.events
    .addListener(
      chart,
      'onmouseover',
      function()
      {
        $("#impChartCanvas").css({"cursor":"pointer"});
      }
    );
  google.visualization.events
    .addListener(
      chart,
      'onmouseout',
      function()
      {
        $("#impChartCanvas").css({"cursor":""});
      }
    );
}
function getTooltip(dt,imp)
{
  return "<div class='line'><span>Date</span> : "+dt+"</div>"+
         "<div class='line'><span>Impressions</span> : "+imp+"</div>";
}
//--------- chart globals
var subsChart;
var impChart;
function initChart()
{
  Chart.defaults.global.scaleFontFamily= "'Roboto', 'Arial', sans-serif";
    //subscription chart
    subsChart =
    new Chart(
          document
          .getElementById("subsChartCanvas")
          .getContext("2d")
        )
        .StackedBar
        (
          subscriberData,
          {
  			     responsive : true,
             scaleShowGridLines : false,
             scaleFontFamily: "Roboto', sans-serif",
             scaleFontSize: 10,
             scaleFontStyle: "normal",
             scaleFontColor: "#555555",
             scaleLineColor:"#e7f0f3",

             tooltipFillColor: "#fff",
             tooltipFontFamily: "'Roboto', 'Arial', sans-serif",
             tooltipFontSize: 14,
             tooltipFontStyle: "300",
             tooltipFontColor: "#7e94a9",

             tooltipTitleFontFamily: "'Roboto', 'Arial', sans-serif",
             tooltipTitleFontSize: 14,
             tooltipTitleFontStyle: "normal",
             tooltipTitleFontColor: "#111111",

             tooltipYPadding: 20,
             tooltipXPadding: 20,
             tooltipCaretSize: 0,
             tooltipCornerRadius: 4,
             tooltipXOffset: 10,
             tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>",
             multiTooltipTemplate: function(v){ return v.value;},

             customTooltips: function(tooltip) {
               //Tooltip Element
                var tooltipEl = $('#chartjs-tooltip');

                // Hide if no tooltip
                if (!tooltip) {
                    tooltipEl.css({
                        opacity: 0
                    });
                    return;
                }

                // Set caret Position
                //tooltipEl.removeClass('above below');
                //tooltipEl.addClass(tooltip.yAlign);

                var fOpted = subscriberData.datasets[0].data[0];
                var fViewed = subscriberData.datasets[0].data[1];
                var mOpted = subscriberData.datasets[1].data[0];
                var mViewed =subscriberData.datasets[1].data[1];
                // Set Text
                tooltipEl.html(
                  "<div><div class='header'>Opted</div><div class='data'><div>"+fOpted+"</div><div>"+mOpted+"</div></div></div>"+
                  "<div class='sep'><div class='header'>Viewed</div><div class='data'><div>"+fViewed+"</div><div>"+mViewed+"</div></div></div>"
                );


                var os =  $("#subsChartCanvas").offset();
                var lft = parseInt(os.left);
                var tp = parseInt(os.top);


                // Display, position, and set styles for font
                tooltipEl.css({
                    opacity: 1,
                    left: lft+ tooltip.x + 'px',
                    top:  tp + 'px'
                });
             }
    		  }
        );

    //impression chart
    impChart =
    new Chart(
          document
          .getElementById("impChartCanvas")
          .getContext("2d")
        )
        .Line
        (
          impressionData,
          {
            responsive : true,
            scaleShowGridLines : false,
            bezierCurve : false,
            pointDot : false,
            datasetFill : false,
            datasetStrokeWidth : 3,
            scaleFontFamily: "'Roboto', sans-serif",
            scaleFontSize: 10,
            scaleFontStyle: "normal",
            scaleFontColor: "#555555",
            scaleLineColor:"#e7f0f3",
            showTooltips:true,

            /*tooltipFillColor: "#fff",
            tooltipFontFamily: "'Roboto', 'Arial', sans-serif",
            tooltipFontSize: 14,
            tooltipFontStyle: "300",
            tooltipFontColor: "#7e94a9",


            tooltipYPadding: 20,
            tooltipXPadding: 20,
            tooltipCaretSize: 5,
            tooltipCornerRadius: 4,
            tooltipXOffset: 10,

            tooltipTemplate: "<%if (label){%>Date:<%=label%>, <%}%>Impressions:<%= value %>",*/
            customTooltips :
              function(tooltip)
              {

                var tooltipEl = $('#chartjs-tooltip-imp');

                if (!tooltip) {
                    tooltipEl.css({
                        opacity: 0
                    });
                    return;
                }

                tooltipEl.removeClass('above below');
                tooltipEl.addClass(tooltip.yAlign);

                var innerHtml = '';
                /*for (var i = tooltip.labels.length - 1; i >= 0; i--) {
                	innerHtml += [
                		'<div class="chartjs-tooltip-section">',
                		'	<span class="chartjs-tooltip-key" style="background-color:' + tooltip.legendColors[i].fill + '"></span>',
                		'	<span class="chartjs-tooltip-value">' + tooltip.labels[i] + '</span>',
                		'</div>'
                	].join('');
                }*/
                var os =  $("#impChartCanvas").offset();
                var lft = parseInt(os.left) -105;
                var tp = parseInt(os.top);
                //console.log(lft +  tooltip.x + 'px' +" ======="+tp +  tooltip.y + 'px');
                tooltipEl.html(innerHtml);

                tooltipEl.css({
                    opacity: 1,
                    left: lft + tooltip.x + 'px',
                    top: tooltip.y - 10  + 'px',
                    fontFamily: tooltip.fontFamily,
                    fontSize: tooltip.fontSize,
                    fontStyle: tooltip.fontStyle,
                });
            }
          }
        );
}
var pageCountSelectedValue;
function bindPageCountSelector(){
  //------pagination count selector
  $(".pageCountSelect select").on("change",function(){
    var value = $(this).find("option:selected").val();
    pageCountSelectedValue = value;
    $("#dataTable").jsGrid("option", "pageSize", value);
  });
}
function bindCollapse(){
  $("#reportAccordion").on('shown.bs.collapse', function () {
    if( $("#panelDetails").hasClass("in") )
    {
      $(".preview-summary").fadeIn();
    }
  });
  $("#reportAccordion").on('hide.bs.collapse', function () {
    if( $("#panelDetails").hasClass("in") )
    {
      $(".preview-summary").hide();
    }
    else{
    }
  });
}
//------------server error-----------
var hidingInProgress = 0 ;
var isHidden = 1 ;
function serverErrorShow(message)
{
  if(message == undefined || message == "")
    message = "Sorry, something went wrong. Please try later."
  $("#serverError").html(message);
  $(".server-error").animate(
                        {"top":"0px"},
                        500,
                        "swing",
                        function(){
                            isHidden = 0;

                        });
}
function serverErrorHide()
{
  hidingInProgress = 1;
  $(".server-error").animate(
                      {"top":"-50px"},
                      500,
                      "swing",
                      function(){
                        isHidden = 1;
                        hidingInProgress = 0;
                      });
}

//--------progress bar--------------
/*
  for help, please visit - http://ricostacruz.com/nprogress/
  example funcation calls
  NProgress.start();
  NProgress.set(0.4);
  NProgress.inc();
  NProgress.done();
*/
