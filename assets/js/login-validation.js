var formInstance;

(function(document, window, $) {
  'use strict';
  var Site = window.Site;
  $(document).ready(function($) {
    Site.run();
  });

  // Example Validataion Full
  // ------------------------
  (function()
  {
    formInstance = $('#addCampaignForm').formValidation(
      {
        framework: "bootstrap",
        button:
        {
          selector: '#loginSubmit',
          disabled: 'disabled'
        },
        icon: null,
        fields:
        {
          'campName':
          {
            validators:
            {
              notEmpty:
              {
                message: 'The name is required'
              },
              regexp:
              {
                regexp: /^[a-zA-Z0-9]+$/
              }
            }
          },
          'user[password]':
          {
            validators:
            {
              notEmpty:
              {
                message: 'The password is required'
              }
            }
          }
        }
    });
    formInstance.on('success.form.fv', function(e) {
        var fv  = $(e.target).data('formValidation');
        fv.defaultSubmit();
    });
  })();

  //----------binding blur and focus and clear

  //fixing remembered username or password
  $("#userName").val("");
  $("#inputPassword").val("");

  $("body").on("mousemove", function(){
    if(hidingInProgress==0 && isHidden==0)
    {
      hidingInProgress = 1;
      setTimeout(function(){ loginServerErrorHide();}, 1500 );
    }
    else {

    }
  });

})(document, window, jQuery);

//------------server error-----------
var hidingInProgress = 0 ;
var isHidden = 1 ;
var icon = "<span class='warn-icon'></span>";
function loginServerErrorShow(message)
{
  if(message == undefined || message == "")
    message = "Sorry, something went wrong. Please try later."
  $("#serverError").html(icon + message);
  $(".server-error").animate(
                        {"top":"0px"},
                        500,
                        "swing",
                        function(){
                            isHidden = 0;

                        });
}
function loginServerErrorHide()
{
  hidingInProgress = 1;
  $(".server-error").animate(
                      {"top":"-50px"},
                      500,
                      "swing",
                      function(){
                        isHidden = 1;
                        hidingInProgress = 0;
                      });
}

//--------progress bar--------------
/*
  for help, please visit - http://ricostacruz.com/nprogress/
  example funcation calls
  NProgress.start();
  NProgress.set(0.4);
  NProgress.inc();
  NProgress.done();
*/
