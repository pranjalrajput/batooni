
(function() {

  var db = {

    loadData: function(filter) {
      //console.log(filter);
      return $.grep(this.dataRows, function(row) {
         if(filter.StartDate)
         {
           var sDate = row.StartDate.split("/");
           var fsDate = filter.StartDate.split("/");
           var stDateInRange =
                (
                  new Date(sDate[2], sDate[1], sDate[0])
                  - new Date(fsDate[2], fsDate[1], fsDate[0], 0,0,0,0)
                ) > 0 ? true : false ;
         }
         if(filter.EndDate)
         {
           var eDate = row.EndDate.split("/");
           var feDate = filter.EndDate.split("/");
           var eDateInRange =
                (
                  new Date(eDate[2], eDate[1], eDate[0])
                  - new Date(feDate[2], feDate[1], feDate[0], 23,59,59,999)
                ) < 0 ? true : false ;
         }
        var ret =
          (!filter.Client || row.Client.indexOf(filter.Client) > -1)
          &&
          (!filter.Brand || row.Brand.indexOf(filter.Brand) > -1)
          &&
          (!filter.Campaign || row.Campaign.indexOf(filter.Campaign) > -1)
          &&
          (!filter.StartDate || stDateInRange)
          &&
          (!filter.EndDate || eDateInRange)  ;

        return ret;
      });
    },

    insertItem: function(insertingClient) {
      this.dataRows.push(insertingClient);
    },

    updateItem: function(updatingClient) {},

    deleteItem: function(deletingClient) {
      var clientIndex = $.inArray(deletingClient, this.dataRows);
      this.clients.splice(clientIndex, 1);
    }

  };

  window.db = db;


  db.dataRows =
  [
    {
      "Client": "Uniliver India",
      "Brand": "Lux",
      "Campaign": "Win Gold Coin",
      "StartDate": "16/12/2015",
      "EndDate": "30/12/2015",
      "Status": "Live",
      "Budget": "11,90,000"
    },
    {
      "Client": "Pizza Hut Pizza Hut Pizza Hut Pizza Hut Pizza Hut Pizza Hut Pizza Hut ",
      "Brand": "Pizza Hut",
      "Campaign": "Cheese Burst Pizza",
      "StartDate": "1/12/2015",
      "EndDate": "3/12/2015",
      "Status": "Draft",
      "Budget": "11,90,000"
    },
    {
      "Client": "Pizza Hut",
      "Brand": "Pizza Hut",
      "Campaign": "Cheese Burst Pizza",
      "StartDate": "1/2/2015",
      "EndDate": "3/12/2015",
      "Status": "Draft",
      "Budget": "11,90,000"
    },
    {
      "Client": "Pizza Hut",
      "Brand": "Pizza Hut",
      "Campaign": "Cheese Burst Pizza",
      "StartDate": "1/3/2015",
      "EndDate": "3/12/2015",
      "Status": "Draft",
      "Budget": "11,90,000"
    },
    {
      "Client": "Pizza Hut",
      "Brand": "Pizza Hut",
      "Campaign": "Cheese Burst Pizza",
      "StartDate": "1/12/2016",
      "EndDate": "3/12/2015",
      "Status": "Draft",
      "Budget": "11,90,000"
    },
    {
      "Client": "Pizza Hut",
      "Brand": "Pizza Hut",
      "Campaign": "Cheese Burst Pizza",
      "StartDate": "1/11/2016",
      "EndDate": "3/12/2015",
      "Status": "Draft",
      "Budget": "11,90,000"
    },
    {
      "Client": "Pizza Hut",
      "Brand": "Pizza Hut",
      "Campaign": "Cheese Burst Pizza",
      "StartDate": "3/12/2016",
      "EndDate": "3/12/2015",
      "Status": "Draft",
      "Budget": "11,90,000"
    },
    {
      "Client": "Pizza Hut",
      "Brand": "Pizza Hut",
      "Campaign": "Cheese Burst Pizza",
      "StartDate": "1/12/2015",
      "EndDate": "3/12/2015",
      "Status": "Draft",
      "Budget": "11,90,000"
    },
    {
      "Client": "Pizza Hut",
      "Brand": "Pizza Hut",
      "Campaign": "Cheese Burst Pizza",
      "StartDate": "1/12/2015",
      "EndDate": "3/12/2015",
      "Status": "Draft",
      "Budget": "11,90,000"
    },
    {
      "Client": "Pizza Hut",
      "Brand": "Pizza Hut",
      "Campaign": "Cheese Burst Pizza",
      "StartDate": "1/12/2015",
      "EndDate": "3/12/2015",
      "Status": "Draft",
      "Budget": "11,90,000"
    },
    {
      "Client": "Pizza Hut",
      "Brand": "Pizza Hut",
      "Campaign": "Cheese Burst Pizza",
      "StartDate": "1/12/2015",
      "EndDate": "3/12/2015",
      "Status": "Draft",
      "Budget": "11,90,000"
    },
    {
      "Client": "Pizza Hut",
      "Brand": "Pizza Hut",
      "Campaign": "Cheese Burst Pizza",
      "StartDate": "1/12/2015",
      "EndDate": "3/12/2015",
      "Status": "Draft",
      "Budget": "11,90,000"
    },
    {
      "Client": "Pizza Hut",
      "Brand": "Pizza Hut",
      "Campaign": "Cheese Burst Pizza",
      "StartDate": "1/12/2015",
      "EndDate": "3/12/2015",
      "Status": "Draft",
      "Budget": "11,90,000"
    },
    {
      "Client": "Pizza Hut",
      "Brand": "Pizza Hut",
      "Campaign": "Cheese Burst Pizza",
      "StartDate": "1/12/2015",
      "EndDate": "3/12/2015",
      "Status": "Draft",
      "Budget": "11,90,000"
    },
    {
      "Client": "Pizza Hut",
      "Brand": "Pizza Hut",
      "Campaign": "Cheese Burst Pizza",
      "StartDate": "1/12/2015",
      "EndDate": "3/12/2015",
      "Status": "Draft",
      "Budget": "11,90,000"
    },
    {
      "Client": "Pizza Hut",
      "Brand": "Pizza Hut",
      "Campaign": "Cheese Burst Pizza",
      "StartDate": "1/12/2015",
      "EndDate": "3/12/2015",
      "Status": "Draft",
      "Budget": "11,90,000"
    },
    {
      "Client": "Pizza Hut",
      "Brand": "Pizza Hut",
      "Campaign": "Cheese Burst Pizza",
      "StartDate": "1/12/2015",
      "EndDate": "3/12/2015",
      "Status": "Draft",
      "Budget": "11,90,000"
    },
    {
      "Client": "Pizza Hut",
      "Brand": "Pizza Hut",
      "Campaign": "Cheese Burst Pizza",
      "StartDate": "1/12/2015",
      "EndDate": "3/12/2015",
      "Status": "Draft",
      "Budget": "11,90,000"
    },
    {
      "Client": "Pizza Hut",
      "Brand": "Pizza Hut",
      "Campaign": "Cheese Burst Pizza",
      "StartDate": "1/12/2015",
      "EndDate": "3/12/2015",
      "Status": "Draft",
      "Budget": "11,90,000"
    }
  ];



}());
