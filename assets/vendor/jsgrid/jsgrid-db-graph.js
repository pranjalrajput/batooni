
(function() {

  var db = {

    loadData: function(filter) {
      return $.grep(this.dataRows, function(row) {
        /*return
            (!filter.Contry || row.Contry.indexOf(filter.Contry) > -1)
            &&
            (!filter.State || row.State.indexOf(filter.State) > -1)
            &&
            (!filter.City || row.City.indexOf(filter.City) > -1);*/
          return true;
      });
    },

    insertItem: function(insertingClient) {
      this.dataRows.push(insertingClient);
    },

    updateItem: function(updatingClient) {},

    deleteItem: function(deletingClient) {
      var clientIndex = $.inArray(deletingClient, this.dataRows);
      this.dataRows.splice(clientIndex, 1);
    }

  };

  window.db = db;


  db.dataRows =
  [
    {
      "Country": "India",
      "State": "MP",
      "City": "Indore",
      "Impression": "1200"
    },
    {
      "Country": "India",
      "State": "MP",
      "City": "Indore",
      "Impression": "1200"
    },
    {
      "Country": "India",
      "State": "MP",
      "City": "Indore",
      "Impression": "1200"
    },
    {
      "Country": "India",
      "State": "MP",
      "City": "Indore",
      "Impression": "1200"
    },
    {
      "Country": "India",
      "State": "MP",
      "City": "Indore",
      "Impression": "1200"
    }
  ];
}());
